<?php

namespace BNNVARA\Subscription\Domain\ValueObject;

use BNNVARA\Subscription\Domain\Exception\InvalidOptInStatusException;

class DoubleOptInStatus
{
    private string $status;

    public const CONFIRMED = 'Confirmed';
    public const PENDING = 'Pending';
    public const NOT_CONFIRMED = 'NotConfirmed';

    public function __construct(string $status)
    {
        if ($this->isValid($status)) {
            $this->status = $status;
        } else {
            throw new InvalidOptInStatusException(sprintf('%s is not a valid status', $status));
        }
    }

    private function isValid(string $status): bool
    {
        $allowedStatus = [
            self::CONFIRMED,
            self::NOT_CONFIRMED,
            self::PENDING
        ];

        return in_array($status, $allowedStatus);
    }

    public function __toString(): string
    {
        return $this->status;
    }
}