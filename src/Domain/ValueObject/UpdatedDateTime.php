<?php

namespace BNNVARA\Subscription\Domain\ValueObject;

use BNNVARA\Subscription\Domain\Exception\InvalidUpdatedDateTimeException;
use DateTime;

class UpdatedDateTime
{
    private DateTime $datetime;

    public function __construct(DateTime $datetime)
    {
        if ($datetime > new DateTime()) {
            throw new InvalidUpdatedDateTimeException();
        }

        $this->datetime = $datetime;
    }

    public function __toString(): string
    {
        return $this->datetime->format('c');
    }
}