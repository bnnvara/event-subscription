<?php

namespace BNNVARA\Subscription\Domain\ValueObject;

class SubscriptionId
{
    private int $id;

    public function __construct(int $subscriptionId)
    {
        $this->id = $subscriptionId;
    }

    public function __toString(): string
    {
        return (string)$this->id;
    }

    public function getId(): int
    {
        return $this->id;
    }


}