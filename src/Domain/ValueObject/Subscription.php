<?php

namespace BNNVARA\Subscription\Domain\ValueObject;

class Subscription
{
    private ?AccountId $accountId = null;
    private ?SubscriptionId $subscriptionId = null;
    private string $name;
    private bool $isSubscribed;
    private UpdatedDateTime $updatedDateTime;
    private DoubleOptInStatus $doubleOptInStatus;
    private string $emailAddress;

    public function __construct(
        ?AccountId $accountId,
        ?SubscriptionId $subscriptionId,
        string $name,
        bool $isSubscribed,
        UpdatedDateTime $updatedDateTime,
        DoubleOptInStatus $doubleOptInStatus,
        string $emailAddress
    ) {
        $this->accountId = $accountId;
        $this->subscriptionId = $subscriptionId;
        $this->name = $name;
        $this->isSubscribed = $isSubscribed;
        $this->updatedDateTime = $updatedDateTime;
        $this->doubleOptInStatus = $doubleOptInStatus;
        $this->emailAddress = $emailAddress;
    }

    public function getAccountId(): ?AccountId
    {
        return $this->accountId;
    }

    public function getSubscriptionId(): ?SubscriptionId
    {
        return $this->subscriptionId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isSubscribed(): bool
    {
        return $this->isSubscribed;
    }

    public function getUpdatedDateTime(): UpdatedDateTime
    {
        return $this->updatedDateTime;
    }

    public function getDoubleOptInStatus(): DoubleOptInStatus
    {
        return $this->doubleOptInStatus;
    }

    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }
}