<?php

namespace BNNVARA\Subscription\Domain\ValueObject;

class AccountId
{
    private string $accountId;

    public function __construct(string $accountId)
    {
        $this->accountId = $accountId;
    }

    public function __toString(): string
    {
        return $this->accountId;
    }
}