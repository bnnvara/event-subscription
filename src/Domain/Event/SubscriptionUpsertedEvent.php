<?php

namespace BNNVARA\Subscription\Domain\Event;

use BNNVARA\Subscription\Domain\ValueObject\Subscription;

class SubscriptionUpsertedEvent
{
    private $data;

    public function __construct(Subscription $data)
    {
        $this->data = $data;
    }

    public function getData(): Subscription
    {
        return $this->data;
    }
}