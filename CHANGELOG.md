# Changelog
All notable changes to this project will be documented in this file.
Note: Changelogs are for HUMANS, not computers.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2] - 2020-05-19
### Changed
- Added EmailAddress to Subscription ValueObject.

## [0.1.2] - 2020-05-10
### Changed
- Hotfix: Changed the location of the "Allowed" Array in the DoubleOptInStatus ValueObject, otherwise it gets
  serialized by JMSSerializer.

## [0.1.1] - 2020-05-08
### Changed
- Made SubscriptionId nullable for new subscriptions (that do not have an ID).

## [0.1] - 2020-04-29
### Added
- everything :D initial version