<?php

namespace Tests\BNNVARA\Subscription\Domain\ValueObject;

use BNNVARA\Subscription\Domain\ValueObject\SubscriptionId;
use PHPUnit\Framework\TestCase;

class SubscriptionIdTest extends TestCase
{
    /** @test */
    public function aSubscriptionIdCanBeCreated(): void
    {
        $subscriptionId = new SubscriptionId(5243);

        $this->assertInstanceOf(SubscriptionId::class, $subscriptionId);
        $this->assertEquals('5243', (string)$subscriptionId);
        $this->assertEquals(5243, $subscriptionId->getId());
    }
}