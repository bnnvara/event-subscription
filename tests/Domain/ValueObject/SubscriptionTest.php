<?php

namespace Tests\BNNVARA\Subscription\Domain\ValueObject;

use BNNVARA\Subscription\Domain\ValueObject\AccountId;
use BNNVARA\Subscription\Domain\ValueObject\DoubleOptInStatus;
use BNNVARA\Subscription\Domain\ValueObject\Subscription;
use BNNVARA\Subscription\Domain\ValueObject\SubscriptionId;
use BNNVARA\Subscription\Domain\ValueObject\UpdatedDateTime;
use DateTime;
use PHPUnit\Framework\TestCase;

class SubscriptionTest extends TestCase
{
    /** @test
     *  @dataProvider getSubscriptions
     */
    public function aSubscriptionCanBeCreated(
        Subscription $subscription,
        ?string $accountId,
        ?int $subscriptionId,
        string $name,
        string $updated,
        string $doubleOptInStatus,
        string $emailAddress
    ): void {

        if (!is_null($accountId)) {
            $this->assertInstanceOf(AccountId::class, $subscription->getAccountId());
            $this->assertEquals($accountId, (string) $subscription->getAccountId());
        }

        if (!is_null($subscriptionId)) {
            $this->assertInstanceOf(SubscriptionId::class, $subscription->getSubscriptionId());
            $this->assertEquals($subscriptionId, $subscription->getSubscriptionId()->getId());
        }

        $this->assertEquals($accountId, (string)$subscription->getAccountId());

        $this->assertEquals($name, $subscription->getName());
        $this->assertTrue($subscription->isSubscribed());

        $this->assertInstanceOf(UpdatedDateTime::class, $subscription->getUpdatedDateTime());
        $this->assertEquals((new DateTime($updated))->format('c'), (string)$subscription->getUpdatedDateTime());

        $this->assertInstanceOf(DoubleOptInStatus::class, $subscription->getDoubleOptInStatus());
        $this->assertEquals($doubleOptInStatus, (string)$subscription->getDoubleOptInStatus());
        $this->assertEquals($emailAddress, (string)$subscription->getEmailAddress());
    }

    public function getSubscriptions(): array
    {
        return [
            [
                $subscription = new Subscription(
                    new AccountId('1AB345678-1234-1234-123456FG'),
                    new SubscriptionId(1234),
                    "KVK",
                    true,
                    new UpdatedDateTime(new DateTime('2020-05-10 20:00:00')),
                    new DoubleOptInStatus('Confirmed'),
                    'test@example.com'
                ),
                '1AB345678-1234-1234-123456FG',
                1234,
                'KVK',
                '2020-05-10 20:00:00',
                'Confirmed',
                'test@example.com'
            ],
            [
                $subscription = new Subscription(
                    new AccountId('1AB345678-1234-1234-123456FG'),
                    null,
                    "KVK",
                    true,
                    new UpdatedDateTime(new DateTime('2020-05-10 20:00:00')),
                    new DoubleOptInStatus('Confirmed'),
                    'test@example.com'
                ),
                '1AB345678-1234-1234-123456FG',
                null,
                'KVK',
                '2020-05-10 20:00:00',
                'Confirmed',
                'test@example.com'
            ],
            [
                $subscription = new Subscription(
                    null,
                    new SubscriptionId(1234),
                    "KVK",
                    true,
                    new UpdatedDateTime(new DateTime('2020-05-10 20:00:00')),
                    new DoubleOptInStatus('Confirmed'),
                    'test@example.com'
                ),
                null,
                1234,
                'KVK',
                '2020-05-10 20:00:00',
                'Confirmed',
                'test@example.com'
            ],
            [
                $subscription = new Subscription(
                    null,
                    null,
                    "KVK",
                    true,
                    new UpdatedDateTime(new DateTime('2020-05-10 20:00:00')),
                    new DoubleOptInStatus('Confirmed'),
                    'test@example.com'
                ),
                null,
                null,
                'KVK',
                '2020-05-10 20:00:00',
                'Confirmed',
                'test@example.com'
            ]
        ];
    }
}