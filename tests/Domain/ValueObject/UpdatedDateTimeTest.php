<?php

namespace Tests\BNNVARA\Subscription\Domain\ValueObject;

use BNNVARA\Subscription\Domain\Exception\InvalidUpdatedDateTimeException;
use BNNVARA\Subscription\Domain\ValueObject\UpdatedDateTime;
use PHPUnit\Framework\TestCase;

class UpdatedDateTimeTest extends TestCase
{
    /** @test */
    public function itCreatesAnUpdatedDateTime(): void
    {
        $datetime = new \DateTime('2020-01-02 12:12:12');
        $updated = new UpdatedDateTime($datetime);

        $this->assertInstanceOf(UpdatedDateTime::class, $updated);
        $this->assertEquals($datetime->format('c'), (string)$updated);
    }

    /** @test */
    public function itThrowsAnExceptionIfDateTimeIsInFuture()
    {
        $this->expectException(InvalidUpdatedDateTimeException::class);

        $datetime = new \DateTime();
        $datetime->add(new \DateInterval("P1D"));

        $updated = new UpdatedDateTime($datetime);

    }
}