<?php

namespace Tests\BNNVARA\Subscription\Domain\ValueObject;

use BNNVARA\Subscription\Domain\Exception\InvalidOptInStatusException;
use BNNVARA\Subscription\Domain\ValueObject\DoubleOptInStatus;
use PHPUnit\Framework\TestCase;

class DoubleOptInStatusTest extends TestCase
{
    /** @test */
    public function aConfirmedStatusCanBeCreated(): void
    {
        $status = new DoubleOptInStatus('Confirmed');

        $this->assertInstanceOf(DoubleOptInStatus::class, $status);
    }

    /** @test */
    public function aPendingStatusCanBeCreated(): void
    {
        $status = new DoubleOptInStatus('Pending');

        $this->assertInstanceOf(DoubleOptInStatus::class, $status);
    }

    /** @test */
    public function aNotConfirmedStatusCanBeCreated(): void
    {
        $status = new DoubleOptInStatus('NotConfirmed');

        $this->assertInstanceOf(DoubleOptInStatus::class, $status);
    }

    /** @test */
    public function itThrowsAnExceptionWhenInvalidStatusIsInserted(): void
    {
        $this->expectException(InvalidOptInStatusException::class);
        $this->expectExceptionMessage('INVALID STRING is not a valid status');

        $status = new DoubleOptInStatus('INVALID STRING');
    }
}