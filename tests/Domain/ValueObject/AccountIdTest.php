<?php

namespace Tests\BNNVARA\Subscription\Domain\ValueObject;

use BNNVARA\Subscription\Domain\ValueObject\AccountId;
use PHPUnit\Framework\TestCase;

class AccountIdTest extends TestCase
{
    /** @test */
    public function anAccountIdCanBeCreated(): void
    {
        $accountId = new AccountId('AB12-eg345-3546273546');

        $this->assertInstanceOf(AccountId::class, $accountId);
        $this->assertEquals('AB12-eg345-3546273546', (string)$accountId);
    }
}