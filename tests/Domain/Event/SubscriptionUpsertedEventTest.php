<?php
namespace BNNVARA\Subscription\Domain\Event;

use BNNVARA\Subscription\Domain\ValueObject\Subscription;
use PHPUnit\Framework\TestCase;

class SubscriptionUpsertedEventTest extends TestCase
{
    /** @test */
    public function anSubscriptionUpsertedEventCanBeCreated(): void
    {
        $subscription = $this->getMockedSubscription();

        $event = new SubscriptionUpsertedEvent($subscription);

        $this->assertInstanceOf(SubscriptionUpsertedEvent::class, $event);
        $this->assertInstanceOf(Subscription::class, $event->getData());
    }

    private function getMockedSubscription(): Subscription
    {
        $subscription = $this->getMockBuilder(Subscription::class)->disableOriginalConstructor()->getMock();

        /** @var Subscription $subscription */
        return $subscription;
    }
}
